# Generated by Django 2.0.6 on 2018-09-07 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profil',
            name='job',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='facebook',
            field=models.CharField(blank=True, default='#', max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='instagram',
            field=models.CharField(blank=True, default='#', max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='linkedin',
            field=models.CharField(blank=True, default='#', max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='twitter',
            field=models.CharField(blank=True, default='#', max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='youtube',
            field=models.CharField(blank=True, default='#', max_length=300, null=True),
        ),
    ]
