# Generated by Django 2.0.6 on 2018-10-19 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ao', '0007_project_creation_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ao',
            name='date_limit',
            field=models.DateTimeField(),
        ),
    ]
