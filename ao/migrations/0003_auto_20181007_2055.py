# Generated by Django 2.0.6 on 2018-10-07 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ao', '0002_category_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='ao',
            name='views',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='project',
            name='views',
            field=models.IntegerField(default=0),
        ),
    ]
