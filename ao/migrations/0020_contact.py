# Generated by Django 2.0.6 on 2018-11-04 10:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ao', '0019_aocontacted_pcontacted'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('budget', models.CharField(max_length=10)),
                ('days', models.IntegerField()),
                ('message', models.TextField()),
                ('ao', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ao.AO')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ao.Company')),
                ('project', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ao.Project')),
            ],
        ),
    ]
