# Generated by Django 2.0.6 on 2018-10-30 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ao', '0013_auto_20181029_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='telephone',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
