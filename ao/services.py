from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

from ao.models import Contact


def send_email(contact_id, type_):
    contact = Contact.objects.get(id=contact_id)
    body = get_template('ao/email/contact.html')
    subject = 'Nouvelle offre envoyé à prepos de votre ' + type_
    from_email = contact.company.mail
    context = {
        'contact': contact,
        'type': type_
    }
    html_content = body.render(context)
    msg = EmailMultiAlternatives(subject, '', from_email, [])
    if type_ == 'appel d\'offre':
        msg.to.append(contact.ao.contact_mail)
    else:
        msg.to.append(contact.project.ao.contact_mail)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
